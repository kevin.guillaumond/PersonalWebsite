=====
kpulp
=====

kpulp is a custom Hugo_ theme, based on (and currently not very different from)
pulp_.

.. _Hugo: https://gohugo.io/
.. _pulp: https://github.com/koirand/pulp

This is not really intented to be used by anyone else, but you can add it to
your website by running the following from the base Hugo folder:

.. code-block:: bash

  git submodule add https://gitlab.com/kevin.guillaumond/kpulp themes/kpulp
