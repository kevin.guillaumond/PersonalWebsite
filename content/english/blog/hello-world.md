---
title: "Hello, World!"
date: 2020-05-18
translationKey: "hello-world"
---

Well, here we are! This is my very first blog post.

This website is still very basic, but it's been quite a journey to get here.

# A cool new project

I first got the domain name around May 2017. I had great plans to have my own
website, and a software project that would be useful forever and wouldn't die
after a few months/years.  Unfortunately, when the renewal notice arrived a year
later, there was still nothing at kevinguillaumond.com :(

This was probably the wakeup call I needed. I had thought about what I wanted
the website to be: a collection of ways to contact me and/or check out my work
(email, social media, GitLab/GitHub), some links to projects I've completed, and
a blog where I can write about software. It was time to start thinking about how
to build that website.


# Going static

At that point of the story, in 2018, I'd just finished Michael Hartl's excellent
[Ruby on Rails Tutorial](https://www.railstutorial.org/) and I *really* wanted
to build something with Rails. I was sure I could build a great backend to
handle the blog posts with their tags, comments and everything.

And then I read [Going static](https://brandur.org/fragments/going-static).
This blog post makes very good points in favour of static websites. The most
important thing is that static websites take a lot less time in maintenance.

[The Intrinsic Static Site](https://brandur.org/aws-intrinsic-static) explains
how to deploy a static website easily using AWS, Travis and GitHub.

This was very promising. I decided that my first step would be a simple HTML
page with some links to my different online accounts. I don't have much
experience with building websites, so I chose [HTML 5
Boilerplate](https://html5boilerplate.com/) as my starting point.  It takes care
of a lot of things that I didn't even know existed, so I could just focus on
writing HTML and CSS.


# Releasing to the world

Following the blog post, I created an S3 bucket for storage and set up ACM for
certificate management and CloudFront for content delivery. I chose to replace
GitHub and Travis with GitLab and its built-in CI/CD. This part was extremely
easy thanks to GitLab's [excellent
tutorial](https://about.gitlab.com/blog/2016/08/26/ci-deployment-and-environments/)
on how to deploy a set of HTML files from an S3 bucket.

So there I had it! I had continuous (and nightly) deployment from the master
branch to the production environment, and automatic deplyoment from the other
branches to the staging environment.

The backend engineer in me was very pleased.

And most importantly, there was *finally* something at kevinguillaumond.com!

It was not the best looking website, but I had coded all the HTML/CSS myself, so
it was my baby.


# A blog with Hugo

This one HTML page was a promising start, but it was very simple. I wanted to be
able to add more pages and a blog, with cool features like i18n and tagging. As
fun as it sounds to write my own static website generator, I decided to use
[Hugo](https://gohugo.io).

It's very easy to get started with Hugo - it just works. Better yet, there are
many themes you can install, which means you go *(get it?)* from zero to running
a customized website on your local machine in a matter of minutes. Deploying it
to GitLab Pages doesn't take much longer.

Adding blog posts is trivial with Hugo: just write posts in Markdown (it's the
option that works out of the box, but I'd like to eventually use
[RST](https://docutils.readthedocs.io/en/sphinx-docs/user/rst/quickstart.html))
in the `contents/` folder and *it just works*.

So this is the situation at the time of writing this first post: Hugo with the
[pulp](https://github.com/koirand/pulp/) theme, hosted on GitLab Pages.


# Going multilingual

It was a fantastic idea to start using Hugo, but I have two problems with the way
things are at the moment.

The first problem is that since I'm using Hugo with a premade theme, it no
longer feels like my website. I just took a template and changed some
configuration values.  It was very straightforward, but it didn't feel like I
contributed much to it. Anyone can make the exact same website in a few minutes.

The other problem is that the theme I chose doesn't work very well with
multilingual content. Hugo is very good at i18n and does allow you to write your
pages in different languages, but if I use a theme, I can't do things like
adding a dropdown with flags to navigate between languages.  This is important
because one of the reasons I want to have a blog is that it will allow me to
practice writing in different languages.

The solution to both problems is to stop using a custom theme, and write my own
instead. That way, I get to do my own thing when it comes to what the website
looks like, and Hugo takes care of the content management.

The problem to the solution to the two problems is that I'm going to need to
write more CSS :(

But I'm very happy I got something started! The plan is to add blog posts while
working on the next version of the website.

Stay tuned!
