---
date: 2020-09-27
tags: 
  - cubing
title: "My PLLs"
translationKey: "hello-world"
---

I don't do as much Rubik's cube solving as I used to and I'm afraid I'm going to
eventually forget some algorithms. So I decided to write down the ones I use,
starting with the PLLs.

The U movements between square brackets at the end indicate the AUF (they are
not cube rotations).

# Corners only

| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| A |![A Perm](/img/cubing-algorithms/PLL/pll01.gif) | x' R' D R' U2 R D' R' U2 R2 | |
| A' |![A' Perm](/img/cubing-algorithms/PLL/pll02.gif) | x' R2 U2 R D R' U2 R D' R| Inverse of A |
| E |![E Perm](/img/cubing-algorithms/PLL/pll03.gif) | (Lw' U R D' R' U' R D) x' (Lw' U' R D' R' U R D) | Actually two OLLs in a row |

## Alternatives
| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| E |![E Perm](/img/cubing-algorithms/PLL/pll03b.gif) | z U2 R2 F (R U R' U')3 F' R2 U2 | Longer but very easy to remenber. |

# Edges only

| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| U |![U Perm](/img/cubing-algorithms/PLL/pll07.gif) |R2 U R U R' U' R' U' R' U R' | |
| Usym |![Usym Perm](/img/cubing-algorithms/PLL/pll06.gif) | L2 U' L' U' L U L U L U' L | Left-handed U perm |
| H |![H Perm](/img/cubing-algorithms/PLL/pll05.gif) | M2' U M2' U2 M2' U M2' | M2' with ring finger + middle finger |
| Z |![Z Perm](/img/cubing-algorithms/PLL/pll04.gif) | M' U M2' U M2' U M' U2 M2' [U'] | M2' with ring finger + middle finger |

## Alternatives
| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| H |![H Perm](/img/cubing-algorithms/PLL/pll05.gif) | R2 U2' R U2' R2' U2' R2 U2' R U2 R2' | Better on bigger cubes |  # TODO remove duplicates
| Z |![Z Perm](/img/cubing-algorithms/PLL/pll04b.gif) | R U R' U R' U' R' U R U' R' U' R2' U R [U2] | Better on bigger cubes |

# Two adjacent corners and two edges

| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| T |![T Perm](/img/cubing-algorithms/PLL/pll10.gif) | (R U R' U') R' F R2 U' R' U' (R U R' F') | |
| J |![J Perm](/img/cubing-algorithms/PLL/pll09.gif) | (R U R' F') (R U R' U') R' F R2 U' R' [U']| Same as T but with the last 4 moves now at the beginning |
| Jsym |![Jsym Perm](/img/cubing-algorithms/PLL/pll08.gif) | L' R' U2 R U R' U2 L U' R [U] | |
| F |![F Perm](/img/cubing-algorithms/PLL/pll13.gif) | R' U' F' (R U R' U') R' F R2 U' (R' U' R U) R' U R| T perm with an (R' U' F') setup, and a cancellation at the end |
| R |![R Perm](/img/cubing-algorithms/PLL/pll11.gif) | (R' U2 R U2) (R' F R U R' U') (R' F' R2) [U']| |
| Rsym |![Rsym Perm](/img/cubing-algorithms/PLL/pll12.gif) | (L U2' L' U2') (L F' L' U' L U) (L F L2') [U] | Left-handed R perm |

# The Gs

There is only one algorithm here. The second one is the inverse of the first one
(after replacing y'RU'R' with the equivalent yLU'L). The other two are the
left-hand equivalents.

| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| G |![G Perm](/img/cubing-algorithms/PLL/pll15.gif) | (R2 Uw) (R' U R' U' R Uw') R2' y (L U' L') | FU-FUR same, FUL adjacent |
| G' |![G' Perm](/img/cubing-algorithms/PLL/pll16.gif) | y' (R' U' R) y R2' (Uw R' U R U' R) (Uw' R2') | FU-FUR same, FUL opposite |
| Gsym |![Gsym Perm](/img/cubing-algorithms/PLL/pll14.gif) | (L2' Uw') (L U' L U L' Uw) L2 y' (R U' R') | FU-FUL same, FUR adjacent |
| G'sym |![G'sym Perm](/img/cubing-algorithms/PLL/pll17.gif) | y (L U L') y' L2' (Uw' L U' L' U L') (Uw L2)  | FU-FUL same, FUR opposite |

# Diagonals

| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| V |![V Perm](/img/cubing-algorithms/PLL/pll18.gif) | (R' U R' U') y  (R' F' R2 U') (R' U R' F) (R F) | |
| N |![N Perm](/img/cubing-algorithms/PLL/pll20.gif) | (R' U L' U2' R U' L)2 [U] | |
| Nsym |![Nsym Perm](/img/cubing-algorithms/PLL/pll19.gif) | (L U' R U2 L' U R')2 [U'] | Left-handed N perm |
| Y |![Y Perm](/img/cubing-algorithms/PLL/pll21.gif) | F (R U' R' U') (R U R' F') (R U R' U') (R' F R F')  | |

## Alternatives
| Name      | Image | Algorithm | Comment |
| :-----------: | ----------- | - | - |
| V |![V Perm](/img/cubing-algorithms/PLL/pll18b.gif) | z U' R D (R' U R U') z' (U R' U' L) (U2 R U2 R') | Better for one-handed solves |
