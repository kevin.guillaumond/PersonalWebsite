---
date: 2020-12-25
tags: 
  - cubing
title: "My OLLs"
---

This is part two of my series of posts detailing my cubing algorithms: the OLLs.

There are 57 OLL cases but I sometimes have variations to influence the PLL,
either to put the corners in the correct position, or at least to avoid a
diagonal PLL (ENVY). That makes 71 sequences in total.

"COLL case" means that after the sequence is performed, the corners will be in
the correct position.

"Diag case" means that after the sequence is performed, two opposite corners
will be swapped, leaving an E, N, V or Y PLL.


# Corners only
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 26 | ![#26](/img/cubing-algorithms/OLL/oll26.gif) | (R U R' U) (R U2 R') | The Sune. You can also do an Antisune with your left hand |
| 27 | ![#27](/img/cubing-algorithms/OLL/oll27.gif) | (R U2) (R' U' R U' R') | The Antisune. You can also do a Sune with your left hand |
| 21 | ![#21](/img/cubing-algorithms/OLL/oll21.gif) | R U2' R' U' R U R' U' R U' R' | Two Antisunes in a row, with annulation. COLL when there are two vertical bars |
| 23 | ![#23](/img/cubing-algorithms/OLL/oll23.gif) | (R U2' R' U' R U' R') (L' U2 L U L' U L) | Right antisune + Left antisune |
| 24 | ![#24](/img/cubing-algorithms/OLL/oll24.gif) | Lw' U' R D' R' U R D x' | COLL case when UBL/UBR are adjacent and LUF/UBR the same (or RUF/UBL different) |
| 25 | ![#25](/img/cubing-algorithms/OLL/oll25.gif) | Lw' U R D' R' U' R D x' | Very similar to the previous one. You can probably use the symmetric to avoid ENVY but I don't use it |
| 22 | ![#22](/img/cubing-algorithms/OLL/oll22.gif) | R U2' R2' U' R2 U' R2' U2' R | COLL case when URF/URB are the same and ULF/ULB different |

## Alternatives
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 23 | ![#23](/img/cubing-algorithms/OLL/oll23.gif) | (R U2' R' U' R U' R') U2 (R' U2' R U R' U R) | Same as the defaut one but all RU |
| 23 | ![#23](/img/cubing-algorithms/OLL/oll23.gif) | (R U2' R' U' R U' R') U' (R U' L' U R' U' L)  | Antisune + Niklas. COLL case when URB/URF are adjacent colors and FUL is adjacent to ULB |
| 23 | ![#23b](/img/cubing-algorithms/OLL/oll23b.gif) | R2' D' R U2 R' D R U2 R | COLL case when ULB/URB are the same color and FUL is the same color as ULB |
| 23 | ![#23b](/img/cubing-algorithms/OLL/oll23b.gif) | R' F (R U' R' U') (R U R' F') (R U R' U') (R' F R F') R  | Y perm with an R' setup. COLL case when ULB/URB are the same color and RUF is adjacent |
| 21 | ![#21](/img/cubing-algorithms/OLL/oll21.gif) | F (R U R' U')3 F'| COLL when there are two horizontal bars, which is the diag case with the normal sequence |
| 24 | ![#24](/img/cubing-algorithms/OLL/oll24.gif) | Rw U L' D L U' L' D' x' | Symetric of the other sequence, so it is the COLL when UBL/UBR are adjacent and RUF/UBL the same (the other sequence would result in ENVY)  |
| 22 | ![#22b](/img/cubing-algorithms/OLL/oll22b.gif) | (Rw U R' U) (R' F R F') (R U' R' U) (R U2 Rw') | This is for the diag case (if the bar is on the left instead of the right). This is the COLL. |
| 22 | ![#22](/img/cubing-algorithms/OLL/oll22.gif) | R' U2' R2 U R2' U R2 U2' R | ZBLL time! This is the F/B mirror of the normal sequence, used in the COLL case when FU is the opposite color of URF/URB. This avoid a Z perm (so forces U, U', H or Skip) |

# Bars, part 1: Ts, Cs, and big lignthing bolts
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 33 | ![#33](/img/cubing-algorithms/OLL/oll33.gif) | (R U R' U') (R' F R F') | Beginning of the T perm. COLL case when ULF/ULB different and FUR/ULB same. If different, do U2 + symmetric sequence with your left hand to avoid ENVY and get the COLL |
| 45 | ![#45](/img/cubing-algorithms/OLL/oll45.gif) | F (R U R' U') F'| |
| 46 | ![#46](/img/cubing-algorithms/OLL/oll46.gif) | (R' U') (R' F R F') (U R) | |
| 34 | ![#34](/img/cubing-algorithms/OLL/oll34.gif) | R U R2' U' R' F R U R U' F' | |
| 40 | ![#40](/img/cubing-algorithms/OLL/oll40.gif) | (R' F) (R U R' U') F' U R | |
| 39 | ![#39](/img/cubing-algorithms/OLL/oll39.gif) | (L F') (L' U' L U) F U' L' | L/R mirror of the previous one |

# Bars, part 2: Just the bar
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 51 | ![#51](/img/cubing-algorithms/OLL/oll51.gif) | F (U R U' R')2 F' | |
| 52 | ![#52](/img/cubing-algorithms/OLL/oll52.gif) | (R' U' R U' R' Dw) (R' U R B) | The F/B mirror also works |
| 56 | ![#56](/img/cubing-algorithms/OLL/oll56.gif) | F (R U R' U') R F' Rw U R' U' Rw' | |
| 55 | ![#55](/img/cubing-algorithms/OLL/oll55.gif) | Rw U2 R' U' R Rw' (R U R' U') Rw U' Rw' | Sexy move inside a Fat Antisune, similar to #54 |

# Bars, part 3: Big Ls
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 16 | ![#16](/img/cubing-algorithms/OLL/oll16.gif) | (Rw U Rw') (R U R' U') (Rw U' Rw') | Sexy move with a setup |
| 15 | ![#15](/img/cubing-algorithms/OLL/oll15.gif) | (Lw' U' Lw) (L' U' L U) (Lw' U Lw) | L/R mirror of the previous one |
| 13 | ![#13](/img/cubing-algorithms/OLL/oll13.gif) | Rw U' Rw' U' Rw U Rw' y' R' U R | |
| 14 | ![#14](/img/cubing-algorithms/OLL/oll14.gif) | Lw' U Lw U Lw' U' Lw y L U' L' | L/R mirror of the previous one |

# Adjacent edges, part 1: Small Ls
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 48 | ![#48](/img/cubing-algorithms/OLL/oll48.gif) | F (R U R' U')2 F' | |
| 47 | ![#47](/img/cubing-algorithms/OLL/oll47.gif) | F' (L' U' L U) F | L/R mirror of the previous one |
| 54 | ![#54](/img/cubing-algorithms/OLL/oll54.gif) | Rw U2 R' U' (R U R' U') R U' Rw' | Sexy move inside a Fat Antisune, similar to #55 |
| 53 | ![#53](/img/cubing-algorithms/OLL/oll53.gif) | Lw' U2' L U (L' U' L U) L' U Lw | L/R mirror of the previous one |
| 49 | ![#49](/img/cubing-algorithms/OLL/oll49.gif) | R B' R2' F R2 B R2' F' R | |
| 50 | ![#50](/img/cubing-algorithms/OLL/oll50.gif) | Rw' U Rw2 U' Rw2' U' Rw2 U Rw' | Actually the L/R mirror of the previous one, but written in a more right-handed friendly way |

# Adjacent edges, part 2: Fishes and squares
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 37 | ![#37](/img/cubing-algorithms/OLL/oll37.gif) | F (R U' R' U') (R U R' F') | First half of the Y perm |
| 35 | ![#35](/img/cubing-algorithms/OLL/oll35.gif) | R U2 R2 F R F' R U2 R' | |
| 10 | ![#10](/img/cubing-algorithms/OLL/oll10.gif) | (R U R' U) (R' F R F') (R U2 R') | |
| 9 | ![#9](/img/cubing-algorithms/OLL/oll09.gif) | (R U R' U' R' F) (R2 U R' U' F') | |
| 6 | ![#6](/img/cubing-algorithms/OLL/oll06.gif) | Rw U2 R' U' R U' Rw' | Fat Antisune |
| 5 | ![#5](/img/cubing-algorithms/OLL/oll05.gif) | Lw' U2' L U L' U Lw | Fat Antisune on the left hand |

# Adjacent edges, part 3: Ps and Ws
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 44 | ![#44](/img/cubing-algorithms/OLL/oll44.gif) | F (U R U' R') F' | |
| 43 | ![#43](/img/cubing-algorithms/OLL/oll43.gif) | F' (U' L' U L) F | |
| 31 | ![#31](/img/cubing-algorithms/OLL/oll31.gif) | (F R' F' R) (U R U R') (U' R U' R') | |
| 32 | ![#32](/img/cubing-algorithms/OLL/oll32.gif) | (F' L F L') (U' L' U' L) (U L' U L) | L/R mirror of the previous one |
| 38 | ![#38](/img/cubing-algorithms/OLL/oll38.gif) | (R U R' U) (R U' R' U') (R' F R F')| Inverse of #31 |
| 36 | ![#36](/img/cubing-algorithms/OLL/oll36.gif) | (L' U' L U') (L' U L U) (L F' L' F) | L/R mirror of #38 and inverse of #32 |

## Alternatives
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 44 | ![#44b](/img/cubing-algorithms/OLL/oll44b.gif) | Fw (R U R' U') Fw' | Avoids an initial U2 |
| 43 | ![#43b](/img/cubing-algorithms/OLL/oll43b.gif) | Fw' (L' U' L U) Fw | Avoids an initial U2 |

# Adjacent edges, part 3: Awkward shapes
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 41 | ![#41](/img/cubing-algorithms/OLL/oll41.gif) | (R U R' U R U2' R') F (R U R' U') F' | Two known sequences in a row |
| 42 | ![#42](/img/cubing-algorithms/OLL/oll42.gif) | (L' U' L U' L' U2 L) F' (L' U' L U) F | L/R mirror of the previous one |
| 29 | ![#29](/img/cubing-algorithms/OLL/oll29.gif) | (R U R' U') (R U' R') (F' U' F) (R U R') | |
| 30 | ![#30](/img/cubing-algorithms/OLL/oll30.gif) | F U (R U2 R' U')2 F'| |

# Dots
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 2 | ![#2](/img/cubing-algorithms/OLL/oll02.gif) | F (R U R' U') S (R U R' U') Fw' | |
| 1 | ![#1](/img/cubing-algorithms/OLL/oll01.gif) | (R U2) (R2' F R F') U2' (R' F R F') | |
| 19 | ![#19](/img/cubing-algorithms/OLL/oll19.gif) | R Rw' U (R U R' U') Rw R2' F R F' | Similar to #20 |
| 18 | ![#18](/img/cubing-algorithms/OLL/oll18.gif) | Rw U R' U R U2 Rw2' U' R U' R' U2 Rw | |
| 17 | ![#17](/img/cubing-algorithms/OLL/oll17.gif) | (R U R' U) (R' F R F') U2 (R' F R F') | |
| 3 | ![#3](/img/cubing-algorithms/OLL/oll03.gif) | Fw (R U R' U') Fw' U' F (R U R' U') F' | Yellow corner on right side + yellow bar on R |
| 4 | ![#4](/img/cubing-algorithms/OLL/oll04.gif) | Fw (R U R' U') Fw' U F (R U R' U') F' | Same as before, only the middle move changes direction |

# Edges only
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 28 | ![#28](/img/cubing-algorithms/OLL/oll28.gif) | (Rw U R' U') M' (U R U' R') | |
| 57 | ![#57](/img/cubing-algorithms/OLL/oll57.gif) | (R U R' U') M (U R U' Rw') | Inverse of the previous one |
| 20 | ![#20](/img/cubing-algorithms/OLL/oll20.gif) | R Rw' U (R U R' U') Rw2 R2' U R U' Rw' | The least probable case (1/216). Similar to #19 |

## Alternatives (all for the pure orientation)
| Case | Image | Algorithm | Comment |
| ---- | ----------- | - | - |
| 28 | ![#28](/img/cubing-algorithms/OLL/oll28.gif) | (Rw U R' U') Rw' U2 R U R U' R2' U2' R | |
| 57 | ![#57](/img/cubing-algorithms/OLL/oll57.gif) | M' U M' U M' U2 M U M U M U2 | |
| 20 | ![#20](/img/cubing-algorithms/OLL/oll20.gif) | (M' U)4 (M U)4 | |
