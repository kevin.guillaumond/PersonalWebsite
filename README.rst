===================
My personal website
===================

A simple website made with hugo_ and hosted on `GitLab pages`_.

Available at https://kevinguillaumond.com

.. _hugo: https://gohugo.io/
.. _GitLab pages: https://docs.gitlab.com/ee/user/project/pages/

-----------
Run locally
-----------

Run ``hugo server`` and check the output.

The website should be available at ``http://localhost:1313/``

---------------------
Continuous Deployment
---------------------

This website uses GitLab pipelines for continous deployment.

Every commit to master deploys the website automatically to
https://kevin.guillaumond.gitlab.io/PersonalWebsite/

Master branch status: |master-status|_

.. |master-status| image:: https://gitlab.com/kevin.guillaumond/PersonalWebsite/badges/master/pipeline.svg
.. _master-status: https://gitlab.com/kevin.guillaumond/PersonalWebsite/commits/master
